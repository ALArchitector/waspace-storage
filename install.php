<?php
	if($_SERVER['HTTPS'])
		$url='https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		else
		$url='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$parsed=parse_url($url);
	if(isset($_POST['dbhost']) && isset($_POST['dbname']) && isset($_POST['dbuser']) && isset($_POST['dbpass']))
	{
		$dbhost=$_POST['dbhost'];
		$dbname=$_POST['dbname'];
		$dbuser=$_POST['dbuser'];
        $dbpass=$_POST['dbpass'];
		$stpass=$_POST['stpass'];
		$dblink=mysql_connect($dbhost, $dbuser, $dbpass);
		$connected=false;
		if($dblink)
		if(mysql_select_db($dbname)) $connected=true;
		if(!$connected)
		{
			echo('
				<!DOCTYPE html>
				<html>
				<head>
					<title>Установка | Хранилище WaspAce</title>
					<meta charset="UTF-8">
					<meta name="robots" content="index, follow">
					<link type="text/css" rel="stylesheet" href="http://mv3.waspace.net/css/style.css">
				</head>
				<body>
				<div class="main">
					<div class="auth-no-success">
						<div class="sign-block">
							<form name="reg" method="post" action="'.$url.'">
								<div class="title">Установка скрипта</div>
								<div class="set-info-box">
									<div class="icon"></div>
									<div class="text">
										<b>Ошибка!</b> Указанные данные не подходят. Проверьте правильность указанных данных или
										обратитесь к администратору хостинга.
									</div>
								</div>
								<div class="sign-box">
									<div class="label-block">
										<div class="label-text">Хост базы данных</div>
										<div class="text-line">
											<input name="dbhost" type="text" value="" placeholder="Обычно - localhost">
										</div>
									</div>
									<div class="label-block">
										<div class="label-text">Имя базы данных</div>
										<div class="text-line">
											<input name="dbname" type="text" value="" placeholder="Имя БД">
										</div>
									</div>
									<div class="label-block">
										<div class="label-text">Имя пользователя базы данных</div>
										<div class="text-line">
											<input name="dbuser" type="text" value="" placeholder="Имя пользователя БД">
										</div>
									</div>
									<div class="label-block">
										<div class="label-text">Пароль базы данных</div>
										<div class="text-line">
											<input name="dbpass" type="password" value="" placeholder="Пароль">
										</div>
									</div>
									<div class="label-block">
										<div class="label-text">Пароль доступа к хранилищу (не используйте не-латинские символы)</div>
										<div class="text-line">
											<input name="stpass" type="password" value="" placeholder="Пароль для доступа к хранилищу">
										</div>
									</div>
									<button type="submit" class="btn">Установить</button>
									<button type="reset" class="btn gray">Очистить</button>
								</div>
								<div class="sign-other">
									<div class="other-text">
										<b>Что это такое?</b> Данный скрипт предназначен для установки хранилища для пользования
										услугами сервиса WaspAce.
									</div>
									<div class="other-text">
										<b>Не получается установить?</b> Пожалуйста, проверьте введенные данные. Также можно обратиться
										за помощью на
										<a class="link" href="http://forum.waspace.net" target="_blank">форум технической поддержки</a>
										сервиса WaspAce.
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				</body>
				</html>
			');
		}
			else
		{
			$result=mysql_query(
				'CREATE TABLE IF NOT EXISTS `wacookies` (
					userid INTEGER UNSIGNED NOT NULL,
					profilename VARCHAR(255) NOT NULL,
					surferid INTEGER UNSIGNED NOT NULL,
					 uaid INTEGER UNSIGNED NOT NULL,
					 creation_utc BIGINT UNSIGNED NOT NULL,
					 host_key TEXT NOT NULL,
					 name TEXT NOT NULL,
					 value TEXT NOT NULL,
					 path TEXT NOT NULL,
					 expires_utc BIGINT UNSIGNED NOT NULL,
					 secure INTEGER UNSIGNED NOT NULL,
					 httponly INTEGER UNSIGNED NOT NULL,
					 last_access_utc BIGINT UNSIGNED NOT NULL,
					 has_expires INTEGER UNSIGNED NOT NULL DEFAULT 1,
					 persistent INTEGER UNSIGNED NOT NULL DEFAULT 1,
					 priority INTEGER UNSIGNED NOT NULL DEFAULT 1
				)ENGINE=MyISAM;'
			);
			if($result)
			$result=mysql_query(
				'CREATE TABLE IF NOT EXISTS `waoptions` (
					 userid INTEGER UNSIGNED NOT NULL,
					 profilename VARCHAR(255) NOT NULL,
					 surferid INTEGER UNSIGNED NOT NULL,
					 uaid INTEGER UNSIGNED NOT NULL,
					 width INTEGER UNSIGNED NOT NULL,
					 height INTEGER UNSIGNED NOT NULL,
					 cheight INTEGER UNSIGNED NOT NULL DEFAULT 0
				)ENGINE=MyISAM;'
			);
			if($result)
			$result=mysql_query(
				'CREATE TABLE IF NOT EXISTS `wastorages` (
				  `StorageID` varchar(32) NOT NULL,
				  `ItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `Data` LONGBLOB NOT NULL,
				  PRIMARY KEY (`ItemID`),
				  KEY `StorageID` (`StorageID`)
				) ENGINE=InnoDB  DEFAULT CHARSET=BINARY AUTO_INCREMENT=10'
			);			
			if ($result)
			{
				mysql_query('CREATE INDEX `cookiesid` ON wacookies(`userid`,`profilename`,`surferid`,`uaid`);');
				mysql_query('CREATE INDEX `optionsid` ON waoptions(`userid`,`profilename`,`surferid`,`uaid`);');				
				file_put_contents('wapsoptions.php',
					'<?php '."\n".
					'	defined("_WAPS") or die("Access denied."); '."\n".
					'	$db_host="'.$dbhost.'"; '."\n".
					'	$db_name="'.$dbname.'"; '."\n".
					'	$db_user="'.$dbuser.'"; '."\n".
					'	$db_password="'.$dbpass.'";'."\n".
					'	$st_pass="'.$stpass.'"; '."\n".
					'	$allowedHosts=array(); '."\n".
					'?>'
				);
				$parsed=parse_url($url);
				echo('
					<!DOCTYPE html>
					<html>
					<head>
						<title>Установка | Хранилище профилей WaspAce</title>
						<meta charset="UTF-8">
						<meta name="robots" content="index, follow">
						<link type="text/css" rel="stylesheet" href="http://mv3.waspace.net/css/style.css">
					</head>
					<body>
					<div class="main">
						<div class="auth-no-success">
							<div class="sign-block">
								<form name="" method="post" action="">
									<div class="title">Завершение установки</div>
									<div class="set-info-box">
										<div class="icon"></div>
										<div class="text">
											<b>Успешно!</b> Вы успешно установили скрипт хранилища WaspAce. <br><br><span style="font-size:1.2em;">Не забудьте удалить файл install.php.</span>
										</div>
									</div>
									<div class="sign-box">
										<div class="label-block">
											<div class="label-text">Адрес вашего хранилища профилей</div>
											<div class="text-line">
												<input name="" type="text" value="'.$parsed['scheme'].'://'.$parsed['host'].dirname($parsed['path']).'/wastorage.php" readonly>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					</body>
					</html>
				');
			}
		}
	}
		else
	{
		echo('
			<!DOCTYPE html>
			<html>
			<head>
				<title>Установка | Хранилище профилей WaspAce</title>
				<meta charset="UTF-8">
				<meta name="robots" content="index, follow">
				<link type="text/css" rel="stylesheet" href="http://mv3.waspace.net/css/style.css">
			</head>
			<body>
			<div class="main">
				<div class="auth-no-success">
					<div class="sign-block">
						<form name="reg" method="post" action="'.$url.'">
							<div class="title">Установка скрипта</div>
							<div class="sign-box">
								<div class="label-block">
									<div class="label-text">Хост базы данных</div>
									<div class="text-line">
										<input name="dbhost" type="text" value="" placeholder="Обычно - localhost">
									</div>
								</div>
								<div class="label-block">
									<div class="label-text">Имя базы данных</div>
									<div class="text-line">
										<input name="dbname" type="text" value="" placeholder="Имя БД">
									</div>
								</div>
								<div class="label-block">
									<div class="label-text">Имя пользователя базы данных</div>
									<div class="text-line">
										<input name="dbuser" type="text" value="" placeholder="Имя пользователя БД">
									</div>
								</div>
								<div class="label-block">
									<div class="label-text">Пароль базы данных</div>
									<div class="text-line">
										<input name="dbpass" type="password" value="" placeholder="Пароль">
									</div>
								</div>
								<div class="label-block">
									<div class="label-text">Пароль доступа к хранилищу (не используйте не-латинские символы)</div>
									<div class="text-line">
										<input name="stpass" type="password" value="" placeholder="Пароль для доступа к хранилищу">
									</div>
								</div>
								<button type="submit" class="btn">Установить</button>
								<button type="reset" class="btn gray">Очистить</button>
							</div>
							<div class="sign-other">
								<div class="other-text">
									<b>Что это такое?</b> Данный скрипт предназначен для установки хранилища профиля для пользования
									услугами сервиса WaspAce.
								</div>
								<div class="other-text">
									<b>Не получается установить?</b> Пожалуйста, проверьте введенные данные. Также можно обратиться
									за помощью на
									<a class="link" href="http://forum.waspace.net" target="_blank">форум технической поддержки</a>
									сервиса WaspAce.
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			</body>
			</html>
		');
	}
 ?>