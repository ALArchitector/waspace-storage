<?php
define("_WAPS", true);
$st_pass="";
include "wapsoptions.php";

class db
{
	var $db_id = false;
	var $connected = false;
	var $query_num = 0;
	var $query_list = array();
	var $mysql_error = '';
	var $mysql_version = '';
	var $mysql_error_num = 0;
	var $mysql_extend = "MySQL";
	var $MySQL_time_taken = 0;
	var $query_id = false;
	
	function connect($db_user, $db_pass, $db_name, $db_location = 'localhost', $show_error=1)
	{
		if(!$this->db_id = @mysql_connect($db_location, $db_user, $db_pass)) {
			if($show_error == 1) {
				$this->display_error(mysql_error(), mysql_errno());
			} else {
				return false;
			}
		} 

		if(!@mysql_select_db($db_name, $this->db_id)) {
			if($show_error == 1) {
				$this->display_error(mysql_error(), mysql_errno());
			} else {
				return false;
			}
		}

		$this->mysql_version = mysql_get_server_info();

		if(!defined('COLLATE'))
		{ 
			define ("COLLATE", "BINARY");
		}

		if (version_compare($this->mysql_version, '4.1', ">=")) mysql_query("/*!40101 SET NAMES '" . COLLATE . "' */");

		$this->connected = true;

		return true;
	}
	
	function query($query, $show_error=true)
	{
		$time_before = $this->get_real_time();
		if(!$this->connected) $this->connect(DBUSER, DBPASS, DBNAME, DBHOST);
		if(!($this->query_id = mysql_query($query, $this->db_id) )) {
			$this->mysql_error = mysql_error();
			$this->mysql_error_num = mysql_errno();
			if($show_error) {
				$this->display_error($this->mysql_error, $this->mysql_error_num, $query);
			}
		}		
		$this->MySQL_time_taken += $this->get_real_time() - $time_before;
		$this->query_num ++;
		return $this->query_id;
	}
	
	function get_row($query_id = '')
	{
		if ($query_id == '') $query_id = $this->query_id;
		return mysql_fetch_assoc($query_id);
	}

	function get_array($query_id = '')
	{
		if ($query_id == '') $query_id = $this->query_id;
		return mysql_fetch_array($query_id);
	}
	
	
	function super_query($query, $multi = false)
	{

		if(!$multi) {

			$this->query($query);
			$data = $this->get_row();
			$this->free();			
			return $data;
		} else {
			$this->query($query);		
			$rows = array();
			while($row = $this->get_row()) {
				$rows[] = $row;
			}
			$this->free();			
			return $rows;
		}
	}
	
	function num_rows($query_id = '')
	{
		if ($query_id == '') $query_id = $this->query_id;
		return mysql_num_rows($query_id);
	}
	
	function insert_id()
	{
		return mysql_insert_id($this->db_id);
	}

	function get_result_fields($query_id = '') {
		if ($query_id == '') $query_id = $this->query_id;
		while ($field = mysql_fetch_field($query_id))
		{
            $fields[] = $field;
		}
		return $fields;
   	}

	function safesql( $source )
	{
		if ($this->db_id) return mysql_real_escape_string ($source, $this->db_id);
		else return mysql_escape_string($source);
	}

	function free( $query_id = '' )
	{
		if ($query_id == '') $query_id = $this->query_id;
		@mysql_free_result($query_id);
	}

	function close()
	{
		@mysql_close($this->db_id);
	}

	function get_real_time()
	{
		list($seconds, $microSeconds) = explode(' ', microtime());
		return ((float)$seconds + (float)$microSeconds);
	}	

	function display_error($error, $error_num, $query = '')
	{
		if($query) {
			// Safify query
			$query = preg_replace("/([0-9a-f]){32}/", "********************************", $query); // Hides all hashes
			$query_str = "$query";
		}
		
                echo '<?xml version="1.0" encoding="iso-8859-1"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>MySQL Fatal Error</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
		<!--
		body {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 10px;
			font-style: normal;
			color: #000000;
		}
		-->
		</style>
		</head>
		<body>';
 		if (!PRODUCTION) echo ("Ошибка! Оранжевый код номер ".$error_num);
                else echo '<font size="4">MySQL Error!</font> 
			<br />------------------------<br />
			<br />
			
			<u>The Error returned was:</u> 
			<br />
				<strong>'.$error.'</strong>

			<br /><br />
			</strong><u>Error Number:</u> 
			<br />
				<strong>'.$error_num.'</strong>
			<br />
				<br />
			
			<textarea name="" rows="10" cols="52" wrap="virtual">'.$query_str.'</textarea><br />';

		echo '</body></html>';
		
		exit();
	}

}


function input_key($input)
{
    global $DB;
    $input=$DB->safesql($input);
    $input=strip_tags($input);
    return $input;
}

$stp="";
if(isset($_REQUEST['stp']))
{
	$stp=$_REQUEST['stp'];
}

if (isset($_REQUEST['a']))
{
	$r="";
	$DB=new db;
	$DB->connect($db_user, $db_password, $db_name,$db_host);
	switch ($_REQUEST['a'])
	{
		case "get":
			if (isset($_REQUEST['uid']) && isset($_REQUEST['pn']) && isset($_REQUEST['sid']) && isset($_REQUEST['uaid']))
			{
				$userid=input_key($_REQUEST['uid']);
				$surferid=input_key($_REQUEST['sid']);
				$pname=input_key($_REQUEST['pn']);
				$uaid=input_key($_REQUEST['uaid']);
				$rez=$DB->query("SELECT * FROM `waoptions` WHERE `userid`='".$userid."' AND `surferid`='".$surferid."' AND `profilename`='".$pname."' AND `uaid`='".$uaid."' LIMIT 1");
				if($DB->num_rows($rez))
				{
					$options=$DB->get_row($rez);
					$r.=$options['width']."|".$options['height']."|".$options['cheight'].'<br>';
				};
				$rez=$DB->query("SELECT * FROM `wacookies` WHERE `userid`='".$userid."' AND `surferid`='".$surferid."' AND `profilename`='".$pname."' AND `uaid`='".$uaid."'");
				if($DB->num_rows($rez))
					for($i=0;$i<$DB->num_rows($rez);$i++)
					{
						$cookie=$DB->get_row($rez);
						$r.=$cookie['creation_utc']."|".$cookie['host_key']."|".$cookie['name']."|".$cookie['value']."|".$cookie['path']."|".$cookie['expires_utc']."|".$cookie['secure']."|".$cookie['httponly']."|".$cookie['last_access_utc']."|".$cookie['has_expires']."|".$cookie['persistent']."|".$cookie['priority'].'<br>';
					}
				$DB->free($rez);
			}
			break;
		case "set":
			if (isset($_REQUEST['uid']) && isset($_REQUEST['pn']) && isset($_REQUEST['sid']) && isset($_REQUEST['uaid']))
			{
				$userid=input_key($_REQUEST['uid']);
				$surferid=input_key($_REQUEST['sid']);
				$pname=input_key($_REQUEST['pn']);
				$uaid=input_key($_REQUEST['uaid']);
				if(isset($_REQUEST['options']))
				{
					$DB->query("DELETE FROM `waoptions` WHERE `userid`=".$userid." AND `surferid`='".$surferid."' AND `profilename`='".$pname."' AND `uaid`='".$uaid."'");
					list($width, $height, $cheight)=explode("|",$_REQUEST['options']);
					$DB->query("INSERT DELAYED INTO `waoptions` (`userid`,`profilename`,`surferid`,`uaid`,`width`,`height`,`cheight`) VALUES ('".$userid."','".$pname."','".$surferid."','".$uaid."','".$width."','".$height."','".$cheight."')");
				}
				$DB->query("DELETE FROM `wacookies` WHERE `userid`=".$userid." AND `surferid`='".$surferid."' AND `profilename`='".$pname."' AND `uaid`='".$uaid."'");
				if (isset($_REQUEST['cookies']))
				{	
					$cookies=explode("<br>",$_REQUEST['cookies']);
					if(count($cookies)>0)
					{
						$SQL="INSERT DELAYED INTO `wacookies` (`userid`,`profilename`,`surferid`,`uaid`,`creation_utc`,`host_key`,`name`,`value`,`path`,`expires_utc`,`secure`,`httponly`,`last_access_utc`,`has_expires`,`persistent`,`priority`) VALUES ";
						$first = true;
						for ($i = 0; $i<count($cookies); $i++)
						{
							list($creation_utc, $host_key, $name, $value, $path, $expires_utc, $secure, $httponly, $last_access_utc, $has_expires, $persistent, $priority)=explode("|",$cookies[$i]);
							if(in_array($host_key, $allowedHosts) || empty($allowedHosts))
							{
								if($first)
								{
									$first = false;
									$SQL.="('".$userid."','".$pname."','".$surferid."','".$uaid."','".$creation_utc."','".$host_key."','".$name."','".$value."','".$path."','".$expires_utc."','".$secure."','".$httponly."','".$last_access_utc."','".$has_expires."','".$persistent."','".$priority."')";
								}
									else
								{
									$SQL.=",('".$userid."','".$pname."','".$surferid."','".$uaid."','".$creation_utc."','".$host_key."','".$name."','".$value."','".$path."','".$expires_utc."','".$secure."','".$httponly."','".$last_access_utc."','".$has_expires."','".$persistent."','".$priority."')";
								}
							}
						}
						if(!$first)
						{
							$DB->query($SQL);
						}
					}
				}
			}
			break;
		default:
	}
	echo $r;
}elseif(isset($_REQUEST['c']))
	if ($stp==$st_pass){
	{
		$r="";
		$DB=new db;
		$DB->connect($db_user, $db_password, $db_name,$db_host);
		switch ($_REQUEST['c'])
		{
			case "get":
				if (isset($_REQUEST['k']))
				{
					$key=input_key($_REQUEST['k'],32);
					if (isset($_REQUEST['i']))
					{
						$id=intval($_REQUEST['i']);
						$SQL="SELECT * FROM `wastorages` WHERE `StorageID`='".$key."' AND `ItemID`='".$id."'";
					}else
						$SQL="SELECT * FROM `wastorages` WHERE `StorageID`='".$key."' ORDER BY RAND() LIMIT 1";
					$rez=$DB->query($SQL);
					if($DB->num_rows($rez))
						for($i=0;$i<$DB->num_rows($rez);$i++)
						{
							$dt=$DB->get_row($rez);
							$r.=$dt['Data']."<br>".$dt['ItemID'];
						}
					$DB->free($rez);
				}
				break;
			case "getall":
				if (isset($_REQUEST['k']))
				{
					$key=input_key($_REQUEST['k'],32);
					$SQL="SELECT * FROM `wastorages` WHERE `StorageID`='".$key."'";
					$rez=$DB->query($SQL);
					if($DB->num_rows($rez))
						for($i=0;$i<$DB->num_rows($rez);$i++)
						{
							$dt=$DB->get_row($rez);
							$r.=$dt['Data']."<br>".$dt['ItemID']."<br><br>";
						}
					$DB->free($rez);
				}
				break;
			case "set":
				if (isset($_REQUEST['d']))
				{
					if (isset($_REQUEST['k']))
						$key=input_key($_REQUEST['k'],32);
					else
						$key=md5($_POST['d']+time());
					$SQL="INSERT INTO `wastorages` (`StorageID`,`ItemID`,`Data`) VALUES ('".$key."',NULL,'".$_REQUEST['d']."')";
					$DB->query($SQL);
					$r.="Key=".$key."<br>Id=".$DB->insert_id();
					$DB->free($rez);					
				}
				break;
			case "set":
				if (isset($_REQUEST['d']))
				{
					if (isset($_REQUEST['k']))
						$key=input_key($_REQUEST['k'],32);
					else
						$key=md5($_POST['d']+time());
					$SQL="INSERT INTO `wastorages` (`StorageID`,`ItemID`,`Data`) VALUES ('".$key."',NULL,'".$_REQUEST['d']."')";
					$DB->query($SQL);
					$r.="Key=".$key."<br>Id=".$DB->insert_id();
					$DB->free($rez);
				}
				break;
			case "setexplode":
				if (isset($_REQUEST['d']))
				{
					if (isset($_REQUEST['k']))
						$key=input_key($_REQUEST['k'],32);
					else
						$key=md5($_POST['d']+time());
					$incomedata=$_REQUEST['d'];
					$delimiter="<dlmtr>";
					$datas=explode($delimiter,$incomedata);
					$SQL="INSERT INTO `wastorages` (`StorageID`,`ItemID`,`Data`) VALUES ";
					$first=true;
					foreach ($datas as $data){
						if($first){
							$first=false;
							$SQL=$SQL."('".$key."',NULL,'".$data."')";
						}else{
							$SQL=$SQL.",('".$key."',NULL,'".$data."')";
						};
					};
					$DB->query($SQL);
					$r.="Key=".$key."<br>Id=".$DB->insert_id();
					$DB->free($rez);
				}
				break;
			case "del":
				if (isset($_REQUEST['k'])) $SQL="DELETE FROM `wastorages` WHERE `StorageID`='".input_key($_REQUEST['k'],32)."'";
				if (isset($_REQUEST['i'])) $SQL="DELETE FROM `wastorages` WHERE `ItemID`='".intval($_REQUEST['i'])."'";
				$DB->query($SQL);
				break;
			default:
		}
		echo $r;
	}
}
?>
